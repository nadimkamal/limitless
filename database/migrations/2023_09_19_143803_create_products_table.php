<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void {
        Schema::create( 'products', function ( Blueprint $table ) {
            $table->id();
            $table->string( 'title' );
            $table->text( 'description' );
            $table->string( 'thumbnail', 50 );
            $table->string( 'sample_video', 50 );
            $table->text( 'learning_objectives' )->nullable(); // What you'll learn
            $table->integer( 'price' );
            $table->boolean( 'ststus' );
            $table->timestamps();
        } );
        DB::statement( 'ALTER TABLE products AUTO_INCREMENT = 22011' );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {
        Schema::dropIfExists( 'products' );
    }
};

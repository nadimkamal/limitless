<?php

namespace App\View\Components\backend\layouts\form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SubmitButton extends Component {
    public $type, $value;
    public function __construct( $type, $value ) {
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View | Closure | string {
        return view( 'components.backend.layouts.form.submit-button' );
    }
}

<?php

use App\Http\Controllers\BatchTeacher\BatchController;
use App\Http\Controllers\BatchTeacher\TeacherController;
use App\Http\Controllers\Mixed\ImageController;
use App\Http\Controllers\Mixed\StudentController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\StudentManagement\CourseController;
use App\Http\Controllers\StudentManagement\RoleController;
use App\Http\Controllers\StudentManagement\UserController;
use Illuminate\Support\Facades\Route;

// Route::get( '/', function () {
//     return view( 'backend.students.dashboard' );
// } );

// Route::get( '/', [StudentController::class, 'dashboard'] )->name( 'students.dashboard' );
// Route::get( '/students/create', [StudentController::class, 'create'] )->name( 'students.add' );
// Route::post( '/students/store', [StudentController::class, 'store'] )->name( 'students.store' );
// Route::get( '/students/show/{st }', [StudentController::class, 'show'] )->name( 'students.show' );
// Route::get( '/students/{studentData}/edit', [StudentController::class, 'edit'] )->name( 'students.edit' );
// Route::patch( '/students/update/{studentData}', [StudentController::class, 'update'] )->name( 'students.update' );
// Route::delete( '/students/{studentData}/delete', [StudentController::class, 'delete'] )->name( 'students.delete' );

Route::controller( StudentController::class )->group( function () {
    Route::get( '/', 'dashboard' )->name( 'students.dashboard' );
    Route::get( '/students/create', 'create' )->name( 'students.add' );
    Route::post( '/students/store', 'store' )->name( 'students.store' );
    Route::get( '/students/show/{studentData}', 'show' )->name( 'students.show' );
    Route::get( '/students/{studentData}/edit', 'edit' )->name( 'students.edit' );
    Route::patch( '/students/update/{studentData}', 'update' )->name( 'students.update' );
    Route::delete( '/students/{studentData}/delete', 'delete' )->name( 'students.delete' );
} );

// Route::get( '/index', function () {
//     return view( 'index' );
// } );

// Route::get( '/dashboard', function () {
//     return view( 'dashboard' );
// } );

Route::controller( ImageController::class )->group( function () {
    Route::get( '/image', 'index' )->name( 'image.index' );
    Route::post( '/image', 'imageUpload' )->name( 'image.upload' );
} );

Route::controller( TeacherController::class )->group( function () {
    Route::get( '/teacher', 'index' )->name( 'teacher.index' );
    Route::post( '/teacher', 'teacherRegistration' )->name( 'teacher.registration' );
} );
Route::controller( BatchController::class )->group( function () {
    Route::get( '/batch', 'create' )->name( 'batch.index' );
    Route::post( '/batch', 'batchCreate' )->name( 'batch.create' );
} );

Route::controller( RoleController::class )->group( function () {
    Route::get( '/role/create', 'create' )->name( 'role.create' );
    Route::post( '/role/store', 'store' )->name( 'role.store' );
} );

Route::controller( UserController::class )->group( function () {
    Route::get( '/user/create', 'create' )->name( 'user.create' );
    Route::post( '/user/store', 'store' )->name( 'user.store' );
} );
Route::controller( CourseController::class )->group( function () {
    Route::get( '/course/create', 'create' )->name( 'course.create' );
    Route::post( '/course/store', 'store' )->name( 'course.store' );
} );
Route::controller( ProductController::class )->group( function () {
    Route::get( '/product/index', 'index' )->name( 'product.index' );
    Route::get( '/product/create', 'create' )->name( 'product.create' );
    Route::post( '/product/store', 'store' )->name( 'product.store' );
} );
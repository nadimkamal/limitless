<div class="input-group my-3 w-75 m-auto pb-2 pt-2">
    <input type="{{ $type }}" class="form-control btn btn-secondary rounded-1" value="{{ $value }}" />
</div>

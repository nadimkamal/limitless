<span class="m-0 p-0 w-75 m-auto text-danger d-flex justify-content-start">
    @error('dob')
        {{ $message }}
    @endError
</span>

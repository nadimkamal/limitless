<div class="input-group pt-2 w-75 m-auto">
    <label class="input-group-text rounded-start">{{ $label }}
        <span class="text-danger px-1">*</span>
    </label>
    <textarea type="{{ $type }}" class="form-control rounded-end rounded-1" name="{{ $name }}"
        rows="{{ $rows }}" cols="{{ $cols }}" placeholder="{{ $placeholder }}"></textarea>
</div>

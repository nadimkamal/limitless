<x-backend.layouts.master>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>Image Upload</h1>
            </div>
            <div class="panel-body">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    {{-- <img src="images/{{ \Session::get('image') }}" alt="uploaded image"> --}}
                @endif
                <form action="{{ route('image.upload') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label" for="image">Image</label>
                        <input type="file" name="image" class="form-control p-1 m-0 @error('image') is invalid @enderror">
                        @error('image')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="btn btn-outline-secondary">Upload</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel-heading text-center">
            <h1>Uploaded Image's Gallery</h1>
        </div>
        <div class="row">
            {{-- <img class="img-thumbnail" src="images/" alt="uploaded image"> --}}
            {{-- @php
                $i = 1;
            @endphp --}}
            @foreach ($imageData as $imgData)
                <div class="col-lg-3 col-md-4 col-sm-12 mb-3">
                    <img class="img-thumbnail mb-1" src="images/{{ $imgData->image_name }}"
                        alt="{{ 'img-' . $imgData->id }}" style="height: 150px; width:100%">
                    {{-- {!! '<b>' . $imgData->image_name . '</b>' !!} --}}
                    {!! '<b>Action: </b>' !!}
                    {!! '<a href="#" class="text-success border-0 mx-1">Show</a>' !!}
                    {!! '<a href="#" class="text-danger border-0 mx-1">Delete</a>' !!}
                    {{-- @php $i++; @endphp --}}
                </div>
            @endforeach
        </div>
    </div>
</x-backend.layouts.master>

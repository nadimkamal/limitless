<x-backend.layouts.master>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h4 class="card-title">Carousel Image Add</h4>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <i class="fa-solid fa-rotate" style="cursor:pointer;" onclick="inputReset('input_form',this)"></i>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <form id="input_form" action="" method="POST" enctype="multipart/form-data">
                @csrf

                {{-- @php
                echo "<pre>";
                    print_r($errors->all());
                echo "</pre>";
            @endphp --}}
                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Image Title:</label>
                    <div class="col-lg-10">
                        <input type="text" name="image_title" value="{{ old('image_title') }}" class="form-control"
                            placeholder="Image Title" maxlength="50">
                        <span class="text-danger">
                            @error('image_title')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Carousel Image:</label>
                    <div class="col-lg-10">
                        <div class="border p-1">
                            <input type="file" name="image" style="width:100%;">
                        </div>
                        <span class="form-text text-muted">Accepted formats: gif, png, jpg. Max file size 512 KB</span>
                        <span class="text-danger">
                            @error('image')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Image Active Date:</label>
                    <div class="col-lg-10">
                        <input type="date" name="active_date" value="{{ old('active_date') }}" class="form-control"
                            placeholder="Image Title" maxlength="50">
                        <span class="text-danger">
                            @error('active_date')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Image Deactive Date:</label>
                    <div class="col-lg-10">
                        <input type="date" name="deactive_date" value="{{ old('deactive_date') }}"
                            class="form-control" placeholder="Image Title" maxlength="50">
                        <span class="text-danger">
                            @error('deactive_date')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-lg-2 col-form-label">Image Description:</label>
                    <div class="col-lg-10">
                        <textarea rows="4" cols="5" name="image_desc" class="form-control" placeholder="Enter your message here"
                            maxlength="100">{{ old('image_desc') }}</textarea>
                        <span class="text-danger">
                            @error('image_desc')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary" name="submit">Submit<i
                            class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
</x-backend.layouts.master>
